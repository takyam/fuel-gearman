<?php
namespace Gearman;

use Config;
use GearmanClient;

class Gearman
{
    const FUEL_GEARMAN_VERSION = '0.0.1';
    const GEARMAN_JOB_NAME = '_fuel_gearman_job';

    private static $_gearman_client = null;

    //default Gearman Server port
    private static $default_port = 4730;

    /**
     * Gearmanクライアントを初期化
     * @throws \FuelException
     */
    public static function _init()
    {
        //load gearman config file(s)
        Config::load('gearman', true);

        if (is_null(static::$_gearman_client)) {
            if (!extension_loaded('gearman')) {
                throw new \FuelException('You need install PECL Gearman extension. see http://php.net/manual/en/book.gearman.php');
            }

            $gearman_client = new GearmanClient();
            $servers = static::get_servers();
            if(empty($servers)){
                $gearman_client->addServer();
            }else{
                $gearman_client->addServers($servers);
            }

            static::$_gearman_client = $gearman_client;
        }
    }

    /**
     * Add Gearman servers by config settings.
     * @return string
     */
    public static function get_servers(){
        //Get config
        $servers = Config::get('gearman.servers', array());

        //Result array
        $server_str_list = array();

        //When defined servers on config
        if(is_array($servers) && count($servers) > 0){
            foreach($servers as $server){

                //When not defined server.host, skip
                if(!array_key_exists('host', $server) || empty($server['host'])){
                    continue;
                }

                //Get port on config or set default port.
                $port = array_key_exists('port', $server) ? $server['port'] : null;
                if(!is_numeric($port)){
                    $port = static::$default_port;
                }

                //Add server string to result array
                $server_str_list[] = $server['host'] . ':' . $port;
            }
        }

        return '' . implode(',', $server_str_list);
    }

    /**
     * Gearmanサーバーにタスクとしてキューを渡します
     * @param string $task 実行したいOilタスク名
     * @param array $params Taskに渡すパラメータを配列で指定。配列のキーは無視され、可変長引数となる。
     * @param bool $do_background バックグラウンドで実行するかどうか。基本的にバックグラウンドで実行してください。
     * @param bool $level_is_low Jobのレベル。Jobがスタックしてる場合、Highのものが優先されるはず。
     * @return mixed
     */
    protected static function _queue($task, $params = array(), $do_background = true, $level_is_low = true)
    {
        if (!is_array($params)) {
            $params = array($params);
        }

        $job_params = serialize(array(
            'task' => $task,
            'params' => $params,
        ));

        $method = 'do';
        $method .= $level_is_low ? 'Low' : 'High';
        $method .= $do_background ? 'Background' : '';

        return static::$_gearman_client->$method(static::GEARMAN_JOB_NAME, $job_params);
    }

    /**
     * Gearmanサーバーにバックグラウンドタスク（Oilコマンド）としてキューを渡します
     * Oilコマンドは返り値を持たないため、返り値を受け取る事はできません
     * @param string $task 実行したいOilタスク名
     * @param array $params Taskに渡すパラメータを配列で指定。配列のキーは無視され、可変長引数となる。
     * @param bool $level_is_low Jobのレベル。Jobがスタックしてる場合、Highのものが優先されるはず。
     * @return mixed
     */
    public static function queue($task, $params = array(), $level_is_low = true)
    {
        return static::_queue($task, $params, true, $level_is_low);
    }
}