<?php

namespace Fuel\Tasks;

use Log\Log;
use Oil\Refine;
use Exception;
use GearmanWorker;

/**
 * GearmanのWorker
 */
class Gearman
{
    /**
     * main process
     */
    public static function run()
    {
        $try_resubmit = \Config::get('gearman.worker.try_resubmit', true);
        $resubmit_interval = \Config::get('gearman.worker.resubmit_interval', 5);

        $worker = new GearmanWorker();
        $servers = \Gearman\Gearman::get_servers();
        //When defined servers on config
        if(empty($servers)){
            //When can't get servers from config, add default(PECL DEFAULT. maybe: localhost:4730) connection.
            $worker->addServer();
        }else{
            //add single server (or multiple servers), use string host:port
            $worker->addServers($servers);
        }

        $worker->addFunction(\Gearman\Gearman::GEARMAN_JOB_NAME, function ($job) use ($try_resubmit, $resubmit_interval){
            try {
                $data = unserialize($job->workload());
                Refine::run($data['task'], $data['params']);
            } catch (Exception $error) {
                $job->sendException($error->getMessage());
                $job->sendFail();
                \Log::error($error->getMessage());

                if($try_resubmit){
                    \Gearman\Gearman::queue($data['task'], $data['params']);
                    sleep($resubmit_interval);
                }
            }
        });
        while ($worker->work()) ;
    }

    public static function help(){
        return implode(PHP_EOL, array(
            '[fuel-gearman (version ' . \Gearman\Gearman::FUEL_GEARMAN_VERSION . ')]',
            'Usage:',
            '    normal run >> $ oil r gearman',
            '    daemon run >> $ oil r gearman &'
        ));
    }
}