<?php
return array(
	'servers' => array(
		array(
			'host' => 'localhost',
			'port' => null,	//Use 4730 when port is null
		),
		//You can set multiple Gearman Servers here.
		//array(
		//	'host' => '192.168.11.11',
		//	'port' => 4730,
		//),
	),
	'worker' => array(
		'on_error' => array(
			'try_resubmit' => true,
			'resubmit_interval' => 5,	//input seconds
		),
	),
);