# Fuel-Gearman

current version: **0.0.3**

Package use gearman on FuelPHP.

The fuel-gearman run Oil tasks when job assigned.

## requirement

* Gearman
	* http://gearman.org/
* PECL Gearman extension
	* http://php.net/manual/en/book.gearman.php
* FuelPHP 1.5 (or higher)

## Usage

1. Launch gearmand on localhost.
1. Install this package on your FuelPHP App.
	* install path: fuel/packages/gearman
1. Add 'gearman' to always_load > packages in config.php.
1. Launch gearman worker.
	* oil r gearman
	* (OR)
	* oil r gearman &
1. Add enqueue task in your app codes.
	* ex) Gearman::queue('robots', 'enqueue test');
	* ex) Gearman::queue('robots:protect');

## History

* **2013/02/13** version: 0.0.3 released
    * Features:
        * Add Retry(Re-submit queue) on catch error option
* **2013/02/11** version: 0.0.2 released
    * Features:
        * Add config file(you can set host and port(s))
        * Support multiple Gearman Servers
* **2013/01/26** version: 0.0.1 released
    * first version

## Author
* **takyam**
	* twitter: [@takyam](http://twitter.com/takyam)
	* github: [takyam-git](https://github.com/takyam-git)
	* bitbucket: [takyam](https://bitbucket.org/takyam)

## License
MIT
